# elsevier

## Interview problem

### Context

You have been asked to build an application to help people plan their train journeys on the Great Western Railway line.
Your job is to implement a journey planner that provides travel times based on a supplied timetable.

## Acceptance Criteria

| Criterion | Given | When | Then |
| --------- | ----- | ---- | ---- |
| *Report Duration Of Journey Between Two Stations* | The 0907 train from Camborne is scheduled to reach Exeter St Davids at 1137 | I ask for the journey time from Camborne to Exeter St Davids if I am at the station at 0907 | I am told it will take 150 minutes |
| *Report Duration For First Available Train* | The 0907, 1023 and 1112 trains from Camborne are scheduled to reach Exeter St Davids at 1137, 1302 and 1357, respectively | I ask for the journey time from Camborne to Exeter St Davids if I am at the station at 1023 | I am told it will take 159 minutes |
| *Report Duration Including Waiting Time On Platform* (Stretch Goal) | The 0944, 1100 and 1150 trains from St Austell are scheduled to reach Par at 0951, 1108 and 1157, respectively | I ask for the journey time from St Austell to Par if I am at the station at 1101 | I am told it will take 56 minutes | 

## Technical Notes

* The full train timetable for testing is provided in the TestData class.
* The journey planner should be written in Java 8/9
* The application should be test-driven (JUnit preferred).