# Journey planner documentation

## Manual

### Requirements

#### **Java JDK 1.8.0<=**

1. Check actual version with the following command: `java -version`. If it is not installed:
    * [Link to download](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
    * [Installation guide](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)
2. Check environmental variables are set properly with command :  
    * [Set](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/) `JAVA_HOME` variable.
    * [Set](https://www.java.com/en/download/help/path.xml) `Path` variable for the installed jdk bin folder.

#### **Apache Maven 3.3.9<=**

1. [Link to download](https://maven.apache.org/download.cgi)
2. [Installation guide](https://maven.apache.org/install.html)

### How to download, build and start JourneyPlanner

#### Download this git repo

You can clone/download directly this repository
1. If you have git bash, you can clone with the following command:  
``~$ git clone https://gitlab.com/mojito317/elsevier.git``
2. If not, you can download the zipped project directly from [this link](https://gitlab.com/mojito317/elsevier/-/archive/master/elsevier-master.zip). You have to unzip it before further steps.

#### Build JourneyPlanner

1. Open command prompt/terminal and change folder to where you downloaded the program:  
``~$ cd [place of download]/elsevier/JourneyPlanner``
2. Build the program with the help of maven:  
``~/elsevier/JourneyPlanner$ mvn package``
    * If everything went well you must get a message with `BUILD SUCCESS` in it, something like this:
    ![build success][build_success]

#### Start JourneyPlanner

1. Open command prompt/terminal and change folder to target/classes:  
``~/elsevier/JourneyPlanner$ cd target/classes``
2. Run main.Main class  
``~/elsevier/JourneyPlanner/target/classes$ java main.Main``

### Usage of JourneyPlanner

JourneyPlanner is working as a command line program (similarly to [pip](https://pip.pypa.io/en/stable/user_guide/)).

#### General help

If you run JourneyPlanner without any commands or with the `help` command, it will show the [general help](src/main/resources/console/generalHelp.txt).

#### Duration command

As [help](src/main/resources/console/durationHelp.txt) of the `duration` command shows, **duration** command **needs 2**
additional **required options to work as you wish**: `--from`/`-f` and `--to`/`-t`.  
These options are needed to get the 
duration of the first train from station 'A' to station 'B'. If you do not set the `--at`/`-a` option, the program will 
show the duration of the first available train between stations from recent time.

#### Stations command

As [help](src/main/resources/console/stationsHelp.txt) of the `stations` command shows: this command gives available stations.
This command has no options.

## Tested environments

* Win10, Java 1.8.0_191, Apache Maven 3.3.9
* Linux 4.13.0-38-generic, Java 1.8.0_121, Apache Maven 3.3.9

[build_success]: doc/pictures/build_success.png "Maven Build Success"