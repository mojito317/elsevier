package services;

import dao.TestTimetableDAO;
import entities.StationList;
import exceptions.NotFoundStationException;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class JourneyServiceTest {

    private JourneyService service;

    private SimpleDateFormat simpleDateFormat;

    @Before
    public void setUp() {
        String pattern = "HHmm";
        simpleDateFormat = new SimpleDateFormat(pattern, Locale.UK);

        service = new JourneyService(new TestTimetableDAO());
    }

    @Test
    public void getDurationBetweenCamborneAndExeterStDavidsAt0907() throws ParseException {
        long time = getDurationBetweenTowns("Camborne","Exeter St Davids","0907");
        assertEquals(150, time);
    }

    @Test
    public void getDurationBetweenCamborneAndExeterStDavidsAt1023() throws ParseException {
        long time = getDurationBetweenTowns("Camborne","Exeter St Davids","1023");
        assertEquals(159, time);
    }

    @Test
    public void getDurationBetweenCamborneAndExeterStDavidsAt2359() throws ParseException {
        long time = getDurationBetweenTowns("Camborne","Exeter St Davids","2359");
        assertEquals(698, time);
    }

    @Test
    public void getDurationBetweenStAustellAndParAT1101() throws ParseException {
        long time = getDurationBetweenTowns("St Austell", "Par", "1101");
        assertEquals(56, time);
    }

    @Test(expected = NotFoundStationException.class)
    public void getDurationBetweenNotKnownTownAndPar() throws ParseException {
        getDurationBetweenTowns("Not known town", "Par", "1101");
    }

    @Test(expected = NotFoundStationException.class)
    public void getDurationBetweenStAustellAndNotKnownTown() throws ParseException {
        getDurationBetweenTowns("St Austell", "Not known town", "1101");
    }

    private long getDurationBetweenTowns(String departureTown, String arrivalTown, String dateInHHmmFormat) throws ParseException {
        return service.getDurationBetweenTwoStations(StationList.getInstance().getStationByName(departureTown),
                StationList.getInstance().getStationByName(arrivalTown), simpleDateFormat.parse(dateInHHmmFormat));
    }

}