package dao;

import data.TimetableFromTestData;
import entities.Train;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({TimetableFromTestData.class})
public class TestTimetableDAOTest {

    @Mock
    TestTimetableDAO dao;

    private static String[][] timetable;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockStatic(TimetableFromTestData.class);
    }

    @Test
    public void getTrains() {
        timetable = new String[][]{
                {"Penzance", "St Erth", "Camborne", "Redruth"}, {"0844", "0854", "0907", "0914"},
                {"1000", "1010", "1023", "1030"}, {"1047", "1057", "1112", "1119"}};
        when(TimetableFromTestData.getTimetable()).thenReturn(timetable);
        dao = new TestTimetableDAO();
        List<Train> trainList = dao.getTrains();

        assertEquals(trainList.size(), timetable.length - 1);

        List<String> gotStationList = new ArrayList<>();

        trainList.get(0).getStationList().forEach(station -> gotStationList.add(station.getName()));
        assertEquals(gotStationList, Arrays.stream(timetable[0]).collect(Collectors.toList()));

        gotStationList.clear();
        trainList.get(1).getStationList().forEach(station -> gotStationList.add(station.getName()));
        assertEquals(gotStationList, Arrays.stream(timetable[0]).collect(Collectors.toList()));

        gotStationList.clear();
        trainList.get(2).getStationList().forEach(station -> gotStationList.add(station.getName()));
        assertEquals(gotStationList, Arrays.stream(timetable[0]).collect(Collectors.toList()));
    }

    @Test
    public void getTrainsWithWrongHHmmFormat() {
        timetable = new String[][]{{"Penzance", "St Erth"}, {"not exactly in HHmm Format", "1010"}};
        when(TimetableFromTestData.getTimetable()).thenReturn(timetable);
        dao = new TestTimetableDAO();

        List<Train> trainList = dao.getTrains();
        assertEquals(trainList.get(0).getStationList().size(), 1);
        assertEquals(trainList.get(0).getStationList().get(0).getName(), "St Erth");
    }
}