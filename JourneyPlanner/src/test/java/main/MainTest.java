package main;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

public class MainTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private String[][] timetable;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Mock
    MainController controller;

    @Test
    public void nothingInCommandLineWillShowGeneralHelp() throws IOException, URISyntaxException {
        URI uri = Main.class.getResource("/console/generalHelp.txt").toURI();
        String output = new String(Files.readAllBytes(Paths.get(uri)));

        String[] args = {};
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithoutOptionsWillShowDurationHelp() throws IOException, URISyntaxException {
        URI uri = Main.class.getResource("/console/durationHelp.txt").toURI();
        String output = new String(Files.readAllBytes(Paths.get(uri)));

        String command = "duration";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithHOptionWillShowDurationHelp() throws IOException, URISyntaxException {
        URI uri = Main.class.getResource("/console/durationHelp.txt").toURI();
        String output = new String(Files.readAllBytes(Paths.get(uri)));

        String command = "duration -h";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithHelpOptionWillShowDurationHelp() throws IOException, URISyntaxException {
        URI uri = Main.class.getResource("/console/durationHelp.txt").toURI();
        String output = new String(Files.readAllBytes(Paths.get(uri)));

        String command = "duration --help";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithHelpOptionWillShowDurationHelpWithOtherOption() throws IOException, URISyntaxException {
        URI uri = Main.class.getResource("/console/durationHelp.txt").toURI();
        String output = new String(Files.readAllBytes(Paths.get(uri)));

        String command = "duration --help --from StationA";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithOnlyF() {
        String output = "You must give \"-f\" or \"--from\\\" and \"-t\" or \"--to\" required options to get duration (see \"help duration\")";
        String command = "duration -f";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithOnlyFrom() {
        String output = "You must give \"-f\" or \"--from\\\" and \"-t\" or \"--to\" required options to get duration (see \"help duration\")";
        String command = "duration --from";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithOnlyT() {
        String output = "You must give \"-f\" or \"--from\\\" and \"-t\" or \"--to\" required options to get duration (see \"help duration\")";
        String command = "duration -t";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithOnlyTo() {
        String output = "You must give \"-f\" or \"--from\\\" and \"-t\" or \"--to\" required options to get duration (see \"help duration\")";
        String command = "duration --to";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void helpWithDurationOptionWillShowDurationHelp() throws IOException, URISyntaxException {
        URI uri = Main.class.getResource("/console/durationHelp.txt").toURI();
        String output = new String(Files.readAllBytes(Paths.get(uri)));

        String command = "help duration";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void helpWithStationsOptionWillShowStationsHelp() throws IOException, URISyntaxException {
        URI uri = Main.class.getResource("/console/stationsHelp.txt").toURI();
        String output = new String(Files.readAllBytes(Paths.get(uri)));

        String command = "help stations";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithNotKnownOptionWillShowWrongUsage() {
        String output = "ERROR: Wrong usage of duration (see \"help duration\")";

        String command = "duration help";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void helpWithNotKnownCommandWillShowUnknownCommand() {
        String output = "unknown command \"something\"";

        String command = "help something";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void helpWithHelpCommandWillShowUnknownCommand() {
        String output = "unknown command \"help\"";

        String command = "help help";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void notKnownCommandWillShowUnknownCommand() {
        String output = "unknown command \"command\"";

        String command = "command";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void helpWithoutOptionWillShowGeneralHelp() throws IOException, URISyntaxException {
        URI uri = Main.class.getResource("/console/generalHelp.txt").toURI();
        String output = new String(Files.readAllBytes(Paths.get(uri)));

        String command = "help";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromToNoGoodUsage() {
        String output = "ERROR: Wrong usage of duration (see \"help duration\")";

        String command = "duration --from --to A";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationWithNotExistingOption() {
        String output = "ERROR: No such option: --f";

        String command = "duration --f";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromAToB() {
        String output = "A station was not found. You can check available stations with the \"stations\" command.";

        String command = "duration --from A --to B";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }


    @Test
    public void durationFAToB() {
        String output = "A station was not found. You can check available stations with the \"stations\" command.";

        String command = "duration -f A --to B";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFATB() {
        String output = "A station was not found. You can check available stations with the \"stations\" command.";

        String command = "duration -f A -t B";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromATB() {
        String output = "A station was not found. You can check available stations with the \"stations\" command.";

        String command = "duration --from A -t B";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromCamborneToB() {
        String output = "B station was not found. You can check available stations with the \"stations\" command.";

        String command = "duration --from Camborne --to B";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromCamborneToCamborne() {
        String output = "Departure station cannot be the same as arrival station: Camborne";

        String command = "duration --from Camborne --to Camborne";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromCamborneToExeterStDavidsAt0907() {
        String output = "Duration of journey between Camborne and Exeter St Davids will take 150 minutes.";
        String command = "duration --from Camborne --to Exeter St Davids --at 09:07";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromCamborneToExeterStDavidsAt1023() {
        String output = "Duration of journey between Camborne and Exeter St Davids will take 159 minutes.";
        String command = "duration --from Camborne --to Exeter St Davids --at 10:23";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromStAustellToParAt1101() {
        String output = "Duration of journey between St Austell and Par will take 56 minutes.";
        String command = "duration --from St Austell --to Par --at 11:01";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromStAustellToParAtWrongTime() {
        String output = "Wrong usage of duration (see \"help duration\")";
        String command = "duration --from St Austell --to Par --at not exactly in HHmm Format";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void durationFromParToStAustellAt1101() {
        String output = "There is no train for your needs.";
        String command = "duration --from Par --to St Austell --at 11:01";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void stations() {
        timetable = new String[][]{{"Penzance", "St Erth", "Camborne", "Redruth", "Truro", "St Austell", "Par",
                "Bodmin Parkway", "Liskeard", "Plymouth", "Exeter St Davids", "Reading", "London Paddington"}};
        String output = String.join("\n", timetable[0]);
        String command = "stations";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void stationsWithHelpOptionWillShowStationsHelp() throws IOException, URISyntaxException {
        URI uri = Main.class.getResource("/console/stationsHelp.txt").toURI();
        String output = new String(Files.readAllBytes(Paths.get(uri)));

        String command = "stations --help";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }

    @Test
    public void stationsWithNotKnownOptionWillShowWrongUsage() {
        String output = "ERROR: Wrong usage of stations (see \"help stations\")";

        String command = "stations help";
        String[] args = command.split(" ");
        Main.main(args);
        assertThat(outContent.toString(), containsString(output));
    }


    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

}