package main;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class MainControllerTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private MainController controller;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        controller = new MainController();
    }

    @Test
    public void printHelpWithNotExistingKindOfHelp() {
        controller.printHelp("notExisting");
        assertThat(outContent.toString(), containsString("Did not load help for notexisting."));
    }
}