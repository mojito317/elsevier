package services;

import dao.DAO;
import entities.Station;
import entities.Train;
import exceptions.SameStationException;

import java.util.*;

import static java.text.MessageFormat.format;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class JourneyService {

    private final List<Train> trains;

    public JourneyService(DAO dao) {
        this.trains = dao.getTrains();
    }

    public long getDurationBetweenTwoStations(Station departureStation, Station arrivalStation, Date requestTime) {
        Train nextTrain = getNextSingleTrainFromStationToStation(departureStation, arrivalStation, requestTime);
        return differenceIncludingWaitingTime(nextTrain.getStopTimeAtStation(arrivalStation), requestTime);
    }

    public long differenceIncludingWaitingTime(Date arrivalTime, Date requestTime) {
        return millisecToMinute(arrivalTime.getTime() - requestTime.getTime());
    }

    public Train getNextSingleTrainFromStationToStation(Station departureStation, Station arrivalStation, Date time) {
        List<Train> nextTrainsFromStationToStation = getNextTrainsFromStationToStationSortedByDepartureTime(departureStation, arrivalStation, time);
        return nextTrainsFromStationToStation.get(0);
    }

    private long millisecToMinute(long milisec) {
        return milisec / 1000 / 60;
    }

    private List<Train> getNextTrainsFromStationToStationSortedByDepartureTime(Station departureStation, Station arrivalStation, Date time) {
        List<Train> nextTrainsFromStationToStation = new ArrayList<>();
        getAllTrainsFromStationToStation(departureStation, arrivalStation).forEach(train -> {
            if (train.getStopTimeAtStation(departureStation).before(time)) {
                LinkedHashMap<Station, Date> tomorrowTrainLinkedHashMap = train.getAtStation();
                tomorrowTrainLinkedHashMap.replaceAll((Station key, Date value) -> new Date(value.getTime() + (1000 * 60 * 60 * 24)));
                nextTrainsFromStationToStation.add(new Train(tomorrowTrainLinkedHashMap));
            } else {
                nextTrainsFromStationToStation.add(train);
            }
        });
        nextTrainsFromStationToStation.sort(comparing(nextTrain -> nextTrain.getStopTimeAtStation(departureStation)));
        return nextTrainsFromStationToStation;
    }

    private List<Train> getAllTrainsFromStationToStation(Station departureStation, Station arrivalStation) {
        if (departureStation.equals(arrivalStation)) {
            throw (new SameStationException(format("Departure station cannot be the same as arrival station: {0}", departureStation.getName())));
        }
        return getAllTrainsFromStation(departureStation).stream()
                .filter(train -> train.getNextStationsFromStation(departureStation).contains(arrivalStation))
                .collect(toList());
    }

    private List<Train> getAllTrainsFromStation(Station departureStation) {
        return trains.stream()
                .filter(train -> train.getStationList().contains(departureStation))
                .collect(toList());
    }
}
