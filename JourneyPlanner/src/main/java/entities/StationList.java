package entities;

import exceptions.NotFoundStationException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.text.MessageFormat.format;

public class StationList {
    private static StationList stationList = new StationList();

    private static List<Station> stations = new ArrayList<>();

    private StationList() {}

    public static StationList getInstance() {
        return stationList;
    }

    public void addToStationList(Station station) {
        boolean alreadyAdded = false;
        for (Station s : stations) {
            if (s.getName().equals(station.getName())) {
                alreadyAdded = true;
            }
        }
        if (!alreadyAdded) {
            StationList.getInstance().getStations().add(station);
        }
    }

    public Station getStationByName(String name) {
        for (Station s : stations) {
            if (s.getName().equals(name)) {
                return s;
            }
        }
        throw new NotFoundStationException(format("{0} station was not found.", name));
    }

    public List<String> getStationNames() {
        return stations.stream()
                .map(station -> station.getName())
                .collect(Collectors.toList());
    }

    private List<Station> getStations() {
        return stations;
    }

}
