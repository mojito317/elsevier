package entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public class Train {

    private final LinkedHashMap<Station, Date> atStation;

    private final List<Station> stations;

    public Train(LinkedHashMap<Station, Date> atStation) {
        this.atStation = atStation;
        this.stations = new ArrayList<>(atStation.keySet());
    }

    public List<Station> getNextStationsFromStation(Station station) {
        List<Station> nextStations = new ArrayList<>();
        boolean fromStationReached = false;
        for (Station s : stations) {
            if (fromStationReached) {
                nextStations.add(s);
            }
            if (station.equals(s)) {
                fromStationReached = true;
            }
        }
        return nextStations;
    }

    public List<Station> getStationList() {
        return this.stations;
    }

    public Date getStopTimeAtStation(Station station) {
        return atStation.get(station);
    }

    public LinkedHashMap<Station, Date> getAtStation() {
        return atStation;
    }
}
