package main.commandline;

public enum DurationOption {
    FROM("-f", "--from"),
    TO("-t", "--to"),
    AT("-a", "--at"),
    HELP("-h", "--help");

    private final String shortString;
    private final String longString;

    DurationOption(String shortString, String longString) {
        this.shortString = shortString;
        this.longString = longString;
    }

    public String getLongName() {
        return longString;
    }

    public String getShortName() {
        return shortString;
    }
}
