package main;

import dao.TestTimetableDAO;
import entities.Station;
import entities.StationList;
import entities.Train;
import exceptions.NotFoundStationException;
import exceptions.SameStationException;
import main.commandline.DurationOption;
import services.JourneyService;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static java.text.MessageFormat.format;

class MainController {

    private JourneyService journeyService;
    private StationList stationList;

    MainController() {
        stationList = StationList.getInstance();
        journeyService = new JourneyService(new TestTimetableDAO());
    }

    void printError(String msg) {
        System.out.println("ERROR: " + msg);
    }

    void printHelp(String kindOfHelp) {
        kindOfHelp = kindOfHelp.toLowerCase();
        try {
            URI uri = Main.class.getResource(format("/console/{0}Help.txt", kindOfHelp)).toURI();
            System.out.println(new String(Files.readAllBytes(Paths.get(uri))));
        } catch (Exception e) {
            printError(format("Did not load help for {0}.", kindOfHelp));
        }
    }

    void printDuration(HashMap<String, String> options) {
        try {
            HashMap<DurationOption, String> durationOptions = getDurationOptionHashMap(options);

            Date at = getDurationAtOption(durationOptions);
            if (requiredOptionsForDurationAreExistingAndWellFormatted(durationOptions)) {
                answerForRequest(durationOptions.get(DurationOption.FROM), durationOptions.get(DurationOption.TO), at);
            }
        } catch (ParseException e) {
            printError("Wrong usage of duration (see \"help duration\")");
        } catch (IllegalArgumentException e) {
            printError(e.getMessage());
        }
    }

    void printStations() {
        System.out.println(String.join("\n", stationList.getStationNames()));
    }

    private boolean requiredOptionsForDurationAreExistingAndWellFormatted(HashMap<DurationOption,
            String> durationOptions) {
        if (!durationOptions.keySet().contains(DurationOption.FROM) ||
                !durationOptions.keySet().contains(DurationOption.TO)) {
            printError("You must give \"-f\" or \"--from\\\" and \"-t\" or \"--to\" required options to get " +
                    "duration (see \"help duration\")");
            return false;
        } else if (durationOptions.get(DurationOption.FROM).isEmpty() ||
                durationOptions.get(DurationOption.TO).isEmpty()) {
            printError("Wrong usage of duration (see \"help duration\")");
            return false;
        } else {
            return true;
        }
    }

    private Date getDurationAtOption(HashMap<DurationOption, String> durationOptions) throws ParseException {
        if (durationOptions.keySet().contains(DurationOption.AT)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            return dateFormat.parse(durationOptions.get(DurationOption.AT));
        }

        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
        return localDateFormat.parse(localDateFormat.format(new Date()));
    }

    private HashMap<DurationOption, String> getDurationOptionHashMap(HashMap<String, String> durationOptions)
            throws IllegalArgumentException {
        HashMap<DurationOption, String> durationOptionHashMap = new HashMap<>();
        for (Map.Entry<String, String> entry : durationOptions.entrySet()) {
            DurationOption durationOption = getDurationOptionByString(entry.getKey());
            durationOptionHashMap.put(durationOption, entry.getValue());
        }
        return durationOptionHashMap;
    }

    private DurationOption getDurationOptionByString(String s) throws IllegalArgumentException {
        for (DurationOption durationOption : DurationOption.values()) {
            if (durationOption.getLongName().equals(s) || durationOption.getShortName().equals(s)) {
                return durationOption;
            }
        }
        throw new IllegalArgumentException(format("No such option: {0}", s));
    }

    private void answerForRequest(String departureStationString, String arrivalStationString, Date requestTime) {
        try {
            Station departureStation = stationList.getStationByName(departureStationString);
            Station arrivalStation = stationList.getStationByName(arrivalStationString);
            Train nextTrain = journeyService.getNextSingleTrainFromStationToStation(departureStation, arrivalStation,
                    requestTime);
            printNextTrainAndDuration(nextTrain, departureStation, arrivalStation, requestTime);
        } catch (IndexOutOfBoundsException e) {
            printError("There is no train for your needs.");
        } catch (NotFoundStationException e) {
            printError(format("{0} You can check available stations with the \"stations\" command.",
                    e.getMessage()));
        } catch (SameStationException e) {
            printError(e.getMessage());
        } catch (Exception e) {
            printError("Something went wrong. Please try again.");
        }
    }

    private void printNextTrainAndDuration(Train nextTrain, Station departureStation, Station arrivalStation, Date requestTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.UK);
        String trainDepartureTimeString = simpleDateFormat.format(nextTrain.getStopTimeAtStation(departureStation).getTime());
        String trainArrivalTimeString = simpleDateFormat.format(nextTrain.getStopTimeAtStation(arrivalStation).getTime());

        String departureStationName = departureStation.getName();
        String arrivalStationName = arrivalStation.getName();

        long durationIncludingWaitingTime = journeyService.differenceIncludingWaitingTime(nextTrain.getStopTimeAtStation(arrivalStation), requestTime);
        System.out.println(format("\nDuration of journey between {1} and {2} will take {4} minutes.\n\n" +
                        "Next available train is:\n" +
                        "| DEPARTURE AT: {0} | FROM: {1} | TO: {2} | ARRIVAL AT: {3} |",
                trainDepartureTimeString, departureStationName, arrivalStationName,
                trainArrivalTimeString, durationIncludingWaitingTime));
    }
}
