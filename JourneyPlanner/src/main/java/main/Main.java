package main;

import exceptions.NotKnownCommandException;
import main.commandline.Command;

import java.util.Arrays;
import java.util.HashMap;

import static java.text.MessageFormat.format;

public class Main {

    public static void main(String[] args) {
        MainController controller = new MainController();
        if (args.length == 0) {
            controller.printHelp("general");
        } else {
            try {
                Command command = getCommandByString(args[0]);
                HashMap<String, String> options = getOptionHashMap(command,
                        Arrays.copyOfRange(args, 1, args.length));
                boolean commandHasOptions = !options.keySet().contains("help") && !options.isEmpty();
                switch (command) {
                    case DURATION:
                        if (commandHasOptions) {
                            controller.printDuration(options);
                        } else if (options.keySet().contains("help") || args.length == 1) {
                            controller.printHelp(command.toString());
                        } else {
                            controller.printError(format("Wrong usage of {0} (see \"help {0}\")", args[0]));
                        }
                        break;
                    case STATIONS:
                        if (args.length == 1) {
                            controller.printStations();
                        } else if (options.keySet().contains("help")) {
                            controller.printHelp(command.toString());
                        } else {
                            controller.printError(format("Wrong usage of {0} (see \"help {0}\")", args[0]));
                        }
                        break;
                    case HELP:
                        if (commandHasOptions) {
                            command = getCommandByString(args[1]);
                            controller.printHelp(command.toString());
                        } else if (args.length > 1) {
                            throw new NotKnownCommandException(format("unknown command \"{0}\"", args[1]));
                        } else {
                            controller.printHelp("general");
                        }
                        break;
                }
            } catch (NotKnownCommandException e) {
                controller.printError(e.getMessage());
            } catch (Exception e) {
                controller.printError("Something went wrong. Please try again.");
            }
        }
    }

    private static HashMap<String, String> getOptionHashMap(Command command, String[] options) {
        HashMap<String, String> optionHashMap = new HashMap<>();

        if (command == Command.HELP && options.length > 0 && !options[0].startsWith("-")) {
            Command sideCommand = getCommandByString(options[0]);
            ;
            if (sideCommand != Command.HELP) {
                optionHashMap.put(sideCommand.toString(), "");
            }
            return optionHashMap;
        }

        for (int i = 0; i < options.length; i++) {

            if (options[i].equals("-h") || options[i].equals("--help")) {
                optionHashMap.clear();
                optionHashMap.put("help", "");
                return optionHashMap;
            } else {
                if (options[i].startsWith("-") || options[i].startsWith("--")) {
                    int j = i + 1;
                    StringBuilder value = new StringBuilder();
                    while (options.length > j && !options[j].startsWith("-")) {
                        value.append(" ").append(options[j]);
                        j++;
                    }
                    optionHashMap.put(options[i], value.toString().trim());
                }
            }
        }
        return optionHashMap;
    }

    private static Command getCommandByString(String commandString) {
        try {
            return Command.valueOf(commandString.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new NotKnownCommandException(format("unknown command \"{0}\"", commandString));
        }
    }
}
