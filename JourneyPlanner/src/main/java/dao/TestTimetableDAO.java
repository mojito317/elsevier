package dao;

import data.TimetableFromTestData;
import entities.Station;
import entities.StationList;
import entities.Train;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.logging.Logger;


import static java.text.MessageFormat.format;

public class TestTimetableDAO implements DAO {

    private final static Logger LOG = Logger.getLogger(TestTimetableDAO.class.getName());

    private final String[][] timetable;

    private static StationList stationList;

    public TestTimetableDAO() {
        stationList = StationList.getInstance();
        this.timetable = TimetableFromTestData.getTimetable();
        setStations();
    }

    public ArrayList getTrains() {
        ArrayList trains = new ArrayList();

        for (int i = 1; i < timetable.length; i++) {
            LinkedHashMap<Station, Date> trainMap = new LinkedHashMap<>();
            for (int j = 0; j < timetable[i].length; j++) {
                Station currentStation = stationList.getStationByName(timetable[0][j]);
                try {
                    String pattern = "HHmm";
                    Date time = new SimpleDateFormat(pattern, Locale.UK).parse(timetable[i][j]);
                    trainMap.put(currentStation, time);
                } catch (ParseException e) {
                    LOG.warning(format("Could not add {0} station to train, because the stop time was not in " +
                            "HHmm format.", currentStation.getName(), e));
                }
            }
            trains.add(new Train(trainMap));
        }
        return trains;
    }

    private void setStations() {
        for (String stationName : timetable[0]) {
            stationList.addToStationList(new Station(stationName));
        }
    }
}
