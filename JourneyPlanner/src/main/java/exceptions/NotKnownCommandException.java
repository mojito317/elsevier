package exceptions;

public class NotKnownCommandException extends RuntimeException {
    public NotKnownCommandException(String message) {
        super(message);
    }
}
