package exceptions;

public class SameStationException extends RuntimeException {

    public SameStationException(String message) {
        super(message);
    }
}